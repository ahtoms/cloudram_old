var WIDTH = 640;
var HEIGHT = 480;

var game_pause = false;
var game_started = false;
var game_over = false;

//Okay, whole duration of the game is 20minutes
class Menu {
  constructor() {
    this.sprites = [ loadImage('sprites/bg/logo.png') ];
    this.on = true;
    this.previous_menu = null;
    this.menu_data = {};
    this.controller_map = null;
    this.renderable_menu = this.main_menu();
  }

  game_over_menu() {
    this.menu_data = {
      menu_ref: this,
    };
    var menu_data = this.menu_data;
    this.controller_map = {
      pause: {
        execute: function(controller) {
          menu_data.menu_ref.renderable_menu = menu_data.menu_ref.main_menu();
          controller.set_target(menu);
          game_pause = false;
          game_started = false;
          game_over = false;
          menu_data.menu_ref.on = true;
          fg.reset();
          bg.reset();
        }
      },
    };

    return function render() {

      noStroke();
      fill('rgb(57,85,9)');
      textAlign(CENTER);
      textSize(28);
      text('GAME OVER', WIDTH/2, HEIGHT/2);
    };
  }

  pause_menu() {
    this.previous_menu = this.pause_menu;
    this.menu_data = {
      menu_ref: this,
      option_index: 0,
      option_limit: 2,
      option_animation_cursor: 1,
      option_color_set: ['rgb(122,154,45)', 'rgb(57,85,9)'],
    };
    var menu_data = this.menu_data;
    this.controller_map = {
      up_down: {
        execute: function(controller) {
          if(menu_data.option_index > 0) {
            menu_data.option_index--;
          }
        }
      },
      down_down: {
        execute: function(controller) {
          if(menu_data.option_index < menu_data.option_limit) {
            menu_data.option_index++;
          }
        }
      },
      pause: {
        execute: function(controller) {
          if(menu_data.option_index === 0) {
            //resume game
            controller.set_target(ram);
            menu.renderable_menu = menu.pause_menu();
            menu.on = false;
            game_pause = false;
          } else if(menu_data.option_index === 1) {
            // options
            menu_data.menu_ref.renderable_menu = menu_data.menu_ref.option_menu();
          } else if(menu_data.option_index === 2) {
            menu_data.menu_ref.renderable_menu = menu_data.menu_ref.main_menu();
            controller.set_target(menu);
            game_pause = false;
            game_started = false;
            menu_data.menu_ref.on = true;
            fg.reset();
            bg.reset();
          }
        }
      },
    };

    return function render() {
      noStroke();
      fill('rgb(57,85,9)');
      image(this.sprites[0], WIDTH/2-(this.sprites[0].width/2), 40);
      //Resume
      noStroke();
      if(this.menu_data.option_index === 0) {
        fill(this.menu_data.option_color_set[this.menu_data.option_animation_cursor % this.menu_data.option_color_set.length]);
        this.menu_data.option_animation_cursor = ((this.menu_data.option_animation_cursor + 2) % 45);
      } else {
        fill('rgb(57,85,9)');
      }
      //rect(WIDTH/2-(180/2), 175, 180, 40);
      textFont(font);
      text('RESUME', WIDTH/2, 235)
      noStroke();
      if(this.menu_data.option_index === 1) {
        fill(this.menu_data.option_color_set[this.menu_data.option_animation_cursor % this.menu_data.option_color_set.length]);
        this.menu_data.option_animation_cursor = ((this.menu_data.option_animation_cursor + 2) % 45);
      } else {
        fill('rgb(57,85,9)');
      }
      //rect(WIDTH/2-(180/2), 250, 180, 40);
      textFont(font);
      text('OPTIONS', WIDTH/2, 300)
      //End game
      noStroke();
      if(this.menu_data.option_index === 2) {
        fill(this.menu_data.option_color_set[this.menu_data.option_animation_cursor % this.menu_data.option_color_set.length]);
        this.menu_data.option_animation_cursor = ((this.menu_data.option_animation_cursor + 2) % 45);
      } else {
        fill('rgb(57,85,9)');
      }
      //rect(WIDTH/2-(180/2), 325, 180, 40);
      textFont(font);
      text('QUIT', WIDTH/2, 365)

    };
  }

  main_menu() {
    this.previous_menu = this.main_menu;
    this.menu_data = {
      menu_ref: this,
      option_index: 0,
      option_limit: 2,
      logo_animation_cursor: 0,
      logo_animation_set: [],
      option_animation_cursor: 1,
      option_color_set: ['rgb(122,154,45)', 'rgb(57,85,9)'],
    };
    var menu_data = this.menu_data;
    this.controller_map = {
      up_down: {
        execute: function(controller) {
          if(menu_data.option_index > 0) {
            menu_data.option_index--;
          }
        }
      },
      down_down: {
        execute: function(controller) {
          if(menu_data.option_index < menu_data.option_limit) {
            menu_data.option_index++;
          }
        }
      },
      pause: {
        execute: function(controller) {
          if(menu_data.option_index == 0) {
            this.renderable_menu = menu_data.menu_ref.pause_menu();
            controller.set_target(ram);
            game_started = true;
            menu_data.menu_ref.on = false;

          } else if(menu_data.option_index == 1) {
            menu_data.menu_ref.renderable_menu = menu_data.menu_ref.option_menu();
          } else if(menu_data.option_index == 2) {
            menu_data.menu_ref.renderable_menu = menu_data.menu_ref.about_menu();
          }
        }
      },
    };

    return function render() {
      if(!game_started) {
        noStroke();
        //fill('rgb(57,85,9)');
        //rect(WIDTH/2-(300/2), 20, 300, 120);
        image(this.sprites[0], WIDTH/2-(this.sprites[0].width/2), 40);

        noStroke();
        if(this.menu_data.option_index === 0) {
          fill(this.menu_data.option_color_set[this.menu_data.option_animation_cursor % this.menu_data.option_color_set.length]);
          this.menu_data.option_animation_cursor = ((this.menu_data.option_animation_cursor + 2) % 45);
        } else {
          fill('rgb(57,85,9)');
        }
        //rect(WIDTH/2-(180/2), 175, 180, 40);
        textFont(font);
        textAlign(CENTER);
        textSize(28);
        text('START', WIDTH/2, 235)

        noStroke();
        if(this.menu_data.option_index === 1) {
          fill(this.menu_data.option_color_set[this.menu_data.option_animation_cursor % this.menu_data.option_color_set.length]);
          this.menu_data.option_animation_cursor = ((this.menu_data.option_animation_cursor + 2) % 45);
        } else {
          fill('rgb(57,85,9)');
        }
        textFont(font);
        text('OPTIONS', WIDTH/2, 300)

        noStroke();
        if(this.menu_data.option_index === 2) {
          fill(this.menu_data.option_color_set[this.menu_data.option_animation_cursor % this.menu_data.option_color_set.length]);
          this.menu_data.option_animation_cursor = ((this.menu_data.option_animation_cursor + 2) % 45);
        } else {
          fill('rgb(57,85,9)');
        }
        textFont(font);
        text('ABOUT', WIDTH/2, 365)
      }
    };
  }

  option_menu() {
    this.menu_data = {
      menu_ref: this,
      option_index: 0,
      option_limit: 1,
      volume_index: 3,
      volume_limit: 3,
      volume_set: [],
      option_animation_cursor: 1,
      option_color_set: ['rgb(122,154,45)', 'rgb(57,85,9)'],
    };
    var menu_data = this.menu_data;
    this.controller_map = {
      left_down: {
        execute: function(controller) {
          if(menu_data.option_index === 0) {

          }
        }
      },
      right_down: {
        execute: function(controller) {
          if(menu_data.option_index === 0) {

          }
        }
      },
      up_down: {
        execute: function(controller) {
          if(menu_data.option_index > 0) {
            menu_data.option_index--;
          }
        }
      },
      down_down: {
        execute: function(controller) {
          if(menu_data.option_index < menu_data.option_limit) {
            menu_data.option_index++;
          }
        }
      },
      pause: {
        execute: function(controller) {
          if(menu_data.option_index === 1) {
            menu_data.menu_ref.renderable_menu = menu_data.menu_ref.previous_menu();
          }
        }
      },
    };

    return function render() {
      noStroke();
      if(this.menu_data.option_index === 0) {
        fill(this.menu_data.option_color_set[this.menu_data.option_animation_cursor % this.menu_data.option_color_set.length]);
        this.menu_data.option_animation_cursor = ((this.menu_data.option_animation_cursor + 2) % 45);
      } else {
        fill('rgb(57,85,9)');
      }
      //rect(60, 100, 180, 40);
      textFont(font);
      textAlign(LEFT);
      textSize(28);
      text('VOLUME', 60, 140)

      //Volume meter
      fill('rgb(57,85,9)');
      rect(450, 75, 100, 100);

      //Keyboard
      noStroke();
      fill('rgb(57,85,9)');
      rect(WIDTH/2-(300/2), 200, 300, 120);

      noStroke();
      if(this.menu_data.option_index === 1) {
        fill(this.menu_data.option_color_set[this.menu_data.option_animation_cursor % this.menu_data.option_color_set.length]);
        this.menu_data.option_animation_cursor = ((this.menu_data.option_animation_cursor + 2) % 45);
      } else {
        fill('rgb(57,85,9)');
      }
      //rect(60, 360, 180, 40);
      textFont(font);
      textAlign(LEFT);
      textSize(28);
      text('BACK', 60, 400)
    };
  }

  about_menu() {
    this.menu_data = {
      menu_ref: this,
      animation_cursor: 0,
    };
    var menu_data = this.menu_data;
    this.controller_map = {
      pause: {
        execute: function(controller) {
          menu_data.menu_ref.renderable_menu = menu_data.menu_ref.main_menu();
        }
      },
    };

    return function render() {
      fill('rgb(57,85,9)');
      textAlign(CENTER);
      textSize(28);
      text('PROGRAMMING, GRAPHICS AND DESIGN: ', WIDTH/2, HEIGHT/2);
      text('@SuperTyred', WIDTH/2, HEIGHT/2);
      text('FONT: PRESS START 2', WIDTH/2, HEIGHT/2);
    };
  }

  render() {
    if(this.on) {
      this.renderable_menu();
    }
  }
}

//Duration of the game is going to be 30minutes approximately,
//Foreground will be the game manager it seems... whatever works :P
class Foreground {
  constructor(ram) {
    this.ram = ram;
    this.reset();
  }

  add(renderable) {
    this.renderables.push(renderable);
  }

  reset() {
    this.start_cloud = new Cloud(WIDTH/2, HEIGHT/2+ 100, Cloud.sprites.length-1, 1)
    this.ram.x = this.start_cloud.x+90;
    this.ram.y = -66;
    this.ram.collision_box_x = this.ram.x + 15;
    this.ram.collision_box_y = (this.ram.y + this.ram.height) - 11;
    this.ram.ram_state.y_moving = true;
    this.ram.y_fall_cursor = this.ram.y_fall_vector.length/2;
    this.start_cloud.deployed = true;
    this.clouds = [
      this.start_cloud,
      new Cloud(-200, -200, 0, 0),
      new Cloud(-200, -200, 0, 0),
      new Cloud(-200, -200, 0, 0),
      new Cloud(-200, -200, 0, 0),
      new Cloud(-200, -200, 0, 0)
    ];
    this.reserved_clouds = [
      this.clouds[0],
      this.clouds[1],
      this.clouds[2],
      this.clouds[3],
      this.clouds[4],
    ];
    this.renderables = [];
    this.max_clouds = 5;
    this.cloud_timer = 170;
    this.cloud_delay_max = 170;
    this.deployed_clouds = 0;
  }

  next_cloud() {
    if(this.cloud_timer >= this.cloud_delay_max) {
      var max_start_y = HEIGHT - 80;
      var min_start_y = 0 + this.ram.height;

      var x_mid = x_start/2;
      var x_velocity = 1;
      var jumps_required = 1;

      var ram_width = this.ram.width;
      var ram_height = this.ram.height;
      var cloud_not_deployed = true;
      var x_start = -116; // largest starts here
      this.cloud_timer = 0;
      while(cloud_not_deployed && this.deployed_clouds < this.max_clouds) {
        var c = this.reserved_clouds.pop();
        c.set(x_start, (min_start_y + (Math.random() * max_start_y)), parseInt(9+(Math.random() * 8)), 1);
        c.deployed = true;
        cloud_not_deployed = false;
        this.deployed_clouds++;
      }
    }
    this.cloud_timer++;
  }

  render() {
    if(game_started) {
      var fgref = this;
      this.renderables.forEach(function(e, index, array) {
        e.render();
      });
      if(ram) {
        if(!game_pause) {
          ram.move();
        }
        ram.render();
      }

      this.clouds.forEach(function(e, index, array) {
        if(e.deployed) {
          var collided = ram.collision(e);
          if(!game_pause) {
            e.move();
          }
          if(collided) {
            if(!ram.on_cloud) {
              ram.on_cloud = true;
              ram.current_cloud = e;
              ram.y_fall_cursor = 0;
              ram.y_velocity = 0;
              ram.y_fall_cursor = 5;
              ram.y = ram.y - ((ram.y + ram.height) - e.collision_box_y) + 8;
              ram.collision_box_y = ram.collision_box_y - ((ram.collision_box_y + ram.collision_box_height) - e.collision_box_y) + 8;
              ram.ram_state.y_moving = false;
              ram.sprite_cursor = ram.sprite_offset;
            }
            if(!ram.ram_state.x_moving_left && !ram.ram_state.x_moving_right) {
              if(!game_pause) {
                ram.x = ram.x + e.x_velocity;
                ram.collision_box_x = ram.collision_box_x + e.x_velocity;
              }
            }
          }
          if(e.x > WIDTH) {
            if(collided) {
              ram.current_cloud = null;
              ram.ram_state.y_moving = true;
              ram.on_cloud = false;
              ram.sprite_cursor = ram.sprite_end-1;
              ram.y_fall_cursor = ram.y_fall_vector.length/2;
            }
            //array.splice(index, 1);
            if(e !== fgref.start_cloud) {
              e.deployed = false;
              fgref.reserved_clouds.push(e);
              fgref.deployed_clouds--;
            } else {
              e.x = -1000;
              e.deployed = false;
              e.x_velocity = 0;
            }

          } else {
            if(game_started) {
              e.render();
            }
          }
        }
      });
      if(!game_pause) {
        this.next_cloud();
      }
    }
  }
}

class Background {
  constructor() {
    this.y_dither_delay = 38;
    this.y_dither_delay_cursor = 0;
    this.y_dither = 4;
    this.y_dither_cursor = 0;
    this.x_start = -200;
    this.x_end = WIDTH;
    this.cloud_x_set = [120, 13, 354, 237, 588, 471, 705, 120, 13, 354, 237, 588, 471, 705];
    this.x_movement = 0;
    this.sun_x = 480;
    this.sun_y = 10;
    this.sprites = [
      loadImage('sprites/bg/clod.png'),
      loadImage('sprites/bg/moon.png'),
      loadImage('sprites/bg/sun.png'),
      loadImage('sprites/bg/clod2.png'),
    ]
    this.background_color = this.set_day();
    this.big_cloud_y = 0;
    this.pixel_diff_big_cloud = 0.0025;
    this.sun_

    this.sun_mov_x = 0;
    this.sun_mov_y = 0;
    this.moon_mov_x = 0;
    this.moon_mov_y = 0;
    this.pixel_diff_sun_y = 0.0025;
    this.pixel_diff_sun_x = 0.0025;
    this.pixel_diff_moon_y = 0.01;
    this.pixel_diff_moon_x = 0.01;

  }

  reset() {
    this.big_cloud_y = 0;
  }

  bg_cloud_seq() {
    image(this.sprites[0], 0)
  }

  set_day() {
    return 'rgb(179,223,85)';
  }

  set_night() {
    return 'rgb(57,85,9)';
  }

  render() {
    background(this.background_color);
    image(this.sprites[2], this.sun_mov_x + this.sun_x, this.sun_mov_y + this.sun_y);
    image(this.sprites[0], (-180 + ((this.cloud_x_set[0] + this.x_movement) % 820)), this.big_cloud_y + 410 + ((1 * (((this.y_dither_cursor % this.y_dither)-1) % 2)) * 2));
    image(this.sprites[0], (-180 + ((this.cloud_x_set[1] + this.x_movement) % 820)), this.big_cloud_y + 410 + ((1 * (((this.y_dither_cursor % this.y_dither)-1) % 2)) * 2));
    image(this.sprites[0], (-180 + ((this.cloud_x_set[2] + this.x_movement) % 820)), this.big_cloud_y + 410 + ((1 * (((this.y_dither_cursor % this.y_dither)-1) % 2)) * 2));
    image(this.sprites[0], (-180 + ((this.cloud_x_set[3] + this.x_movement) % 820)), this.big_cloud_y + 410 + ((1 * (((this.y_dither_cursor % this.y_dither)-1) % 2)) * 2));
    image(this.sprites[0], (-180 + ((this.cloud_x_set[4] + this.x_movement) % 820)), this.big_cloud_y + 410 + ((1 * (((this.y_dither_cursor % this.y_dither)-1) % 2)) * 2));
    image(this.sprites[0], (-180 + ((this.cloud_x_set[5] + this.x_movement) % 820)), this.big_cloud_y + 410 + ((1 * (((this.y_dither_cursor % this.y_dither)-1) % 2)) * 2));
    image(this.sprites[0], (-180 + ((this.cloud_x_set[6] + this.x_movement) % 820)), this.big_cloud_y + 410 + ((1 * (((this.y_dither_cursor % this.y_dither)-1) % 2)) * 2));

    image(this.sprites[3], (-180 + ((this.cloud_x_set[7] + this.x_movement) % 820)), -155 + ((1 * (((this.y_dither_cursor % this.y_dither)-1) % 2)) * 2) - this.big_cloud_y);
    image(this.sprites[3], (-180 + ((this.cloud_x_set[8] + this.x_movement) % 820)), -155 + ((1 * (((this.y_dither_cursor % this.y_dither)-1) % 2)) * 2) - this.big_cloud_y);
    image(this.sprites[3], (-180 + ((this.cloud_x_set[9] + this.x_movement) % 820)), -155 + ((1 * (((this.y_dither_cursor % this.y_dither)-1) % 2)) * 2) - this.big_cloud_y);
    image(this.sprites[3], (-180 + ((this.cloud_x_set[10] + this.x_movement) % 820)), -155 + ((1 * (((this.y_dither_cursor % this.y_dither)-1) % 2)) * 2) - this.big_cloud_y);
    image(this.sprites[3], (-180 + ((this.cloud_x_set[11] + this.x_movement) % 820)), -155 + ((1 * (((this.y_dither_cursor % this.y_dither)-1) % 2)) * 2) - this.big_cloud_y);
    image(this.sprites[3], (-180 + ((this.cloud_x_set[12] + this.x_movement) % 820)), -155 + ((1 * (((this.y_dither_cursor % this.y_dither)-1) % 2)) * 2) - this.big_cloud_y);
    image(this.sprites[3], (-180 + ((this.cloud_x_set[13] + this.x_movement) % 820)), -155 + ((1 * (((this.y_dither_cursor % this.y_dither)-1) % 2)) * 2) - this.big_cloud_y);

    if(!game_pause && game_started && !game_over) {
      this.x_movement++;
      this.big_cloud_y += this.pixel_diff_big_cloud;
      //console.log(this.big_cloud_y);
    }
    this.y_dither_delay_cursor++;
    if(this.y_dither_delay_cursor >= this.y_dither_delay) {
      this.y_dither_cursor = (this.y_dither_cursor+1) % this.y_dither;
      this.y_dither_delay_cursor = 0;
    }
  }
}

class Controller {
  constructor() {
    var self = this;
    this.target = null;
    this.event_triggers = {
      13: { //pause/return
        state: {
        },
        execute: function(keydown) {
          var trigger = self.target.controller_map['pause'];
          if(trigger && keydown) {
            trigger.execute(self);
          }
        }
      },
      37: { //left
        execute: function(keydown) {
          var trigger = null;
          if(keydown) {
            trigger = self.target.controller_map['left_down'];
            if(trigger) {
              trigger.execute(self);
            }
          } else {
            trigger = self.target.controller_map['left_up'];
            if(trigger) {
              trigger.execute(self);
            }
          }
        }
      },
      38: { //up
        execute: function(keydown) {
          var trigger = null;
          if(keydown) {
            trigger = self.target.controller_map['up_down'];
            if(trigger) {
              trigger.execute(self);
            }
          } else {
            trigger = self.target.controller_map['up_up'];
            if(trigger) {
              trigger.execute(self);
            }
          }
        }
      },
      39: { //right
        execute: function(keydown) {
          var trigger = null;
          if(keydown) {
            trigger = self.target.controller_map['right_down'];
              if(trigger) {
                trigger.execute(self);
              }
          } else {
            trigger = self.target.controller_map['right_up'];
            if(trigger) {
              trigger.execute(self);
            }
          }
        }
      },
      40: { //down
        execute: function(keydown) {
          var trigger = null;
          if(keydown) {
            trigger = self.target.controller_map['down_down'];
            if(trigger) {
              trigger.execute(self);
            }
          } else {
            trigger = self.target.controller_map['down_up'];
            if(trigger) {
              trigger.execute(self);
            }
          }
        }
      },
    };
  }

  key_down(keycode) {
      var trigger = this.event_triggers[keycode];
      if(trigger) {
        trigger.execute(true);
      }
  }

  key_up(keycode) {
      var trigger = this.event_triggers[keycode];
      if(trigger) {
        trigger.execute(false);
      }
  }

  set_target(target) {
    this.target = target;
  }
}

class Cloud {
  constructor(x, y, spriteIndex, x_velocity) {
    this.y_dither = 4;
    this.y_change = 0;
    this.y_move = 0;
    this.x = x;
    this.y = y;
    this.y_dither_delay = 0;
    this.y_dither_delay_max = 58;
    this.deployed = false;

    var inst = this;
    this.sprite = loadImage(Cloud.sprites[spriteIndex], function() {
      inst.width = inst.sprite.width;
      inst.height = inst.sprite.height;
      inst.collision_box_x = inst.x + 12;
      inst.collision_box_y = inst.y + inst.height/2;
      inst.collision_box_width = inst.width - 12;
      inst.collision_box_height = 4;
    });
    this.x_velocity = x_velocity;
  }

  set_velocity(velocity) {
    this.x_velocity = velocity;
  }

  move() {
    this.x = this.x + this.x_velocity;
    if(this.y_dither_delay >= this.y_dither_delay_max) {
        this.y_move = (1 * (((this.y_change % this.y_dither)-1) % 2));
        this.y_dither_delay = 0;
    }
    this.y_dither_delay++;
    this.y_change++;
    this.collision_box_x = this.x + 12
    this.collision_box_y = this.y + 20;
  }

  set(x, y, spriteIndex, x_velocity) {
    this.y_dither = 4;
    this.y_change = 0;
    this.y_move = 0;
    this.x = x;
    this.y = y;

    var inst = this;
    this.sprite = loadImage(Cloud.sprites[spriteIndex], function() {
      inst.width = inst.sprite.width;
      inst.height = inst.sprite.height;
      inst.collision_box_x = inst.x + 12;
      inst.collision_box_y = inst.y + inst.height/2;
      inst.collision_box_width = inst.width - 12;
      inst.collision_box_height = 4;
    });
    this.x_velocity = x_velocity;
  }

  render() {
    //Sprite render
    fill('blue');
    noStroke();
    image(this.sprite, this.x, this.y + this.y_move);
  }
}
Cloud.sprites = [
  'sprites/clouds/dark/sprite0.png',
  'sprites/clouds/dark/sprite1.png',
  'sprites/clouds/dark/sprite2.png',
  'sprites/clouds/dark/sprite3.png',
  'sprites/clouds/dark/sprite4.png',
  'sprites/clouds/dark/sprite5.png',
  'sprites/clouds/dark/sprite6.png',
  'sprites/clouds/dark/sprite7.png',
  'sprites/clouds/dark/sprite8.png',
  'sprites/clouds/light/sprite0.png',
  'sprites/clouds/light/sprite1.png',
  'sprites/clouds/light/sprite2.png',
  'sprites/clouds/light/sprite3.png',
  'sprites/clouds/light/sprite4.png',
  'sprites/clouds/light/sprite5.png',
  'sprites/clouds/light/sprite6.png',
  'sprites/clouds/light/sprite7.png',
  'sprites/clouds/light/sprite8.png',
];

class Ram {
  constructor() {
    this.base = 'sprites/ram/';
    this.sprites = [
      loadImage(this.base+'sprite0.png'),
      loadImage(this.base+'sprite1.png'),
      loadImage(this.base+'sprite2.png'),
      loadImage(this.base+'sprite3.png'),
      loadImage(this.base+'sprite4.png'),
      loadImage(this.base+'sprite5.png'),
      loadImage(this.base+'sprite6.png'),
      loadImage(this.base+'sprite7.png'),
      loadImage(this.base+'sprite8.png'),
      loadImage(this.base+'sprite9.png'),
      loadImage(this.base+'sprite10.png'),
      loadImage(this.base+'sprite11.png'),
    ];
    this.efxsprites = [
      loadImage(this.base+'puffld.png'),
      loadImage(this.base+'puffrd.png'),
      loadImage(this.base+'pufflu.png'),
      loadImage(this.base+'puffru.png'),
    ];
    this.sprite_cursor = 0;
    this.sprite_offset = 0;
    this.sprite_end = this.sprites.length/2;
    this.y_fall_cursor = 0;
    this.delay_counter = 0;
    this.delay_max = 8;
    //dodgey but who gives a fucccck!, useful for working out solutions though
    this.y_fall_vector = [
      -10,
      -9, -9,
      -8, -8, -8,
      -7, -7, -7, -7,
      -6, -6, -6, -6, -6,
      -5, -5, -5, -5, -5, -5,
      -4, -4, -4, -4, -4, -4, -4,
      -3, -3,-3, -3, -3, -3, -3, -3,
      -2, -2, -2, -2, -2, -2, -2, -2, -2,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      2, 2, 2, 2, 2, 2, 2, 2, 2,
      3, 3, 3, 3, 3, 3, 3, 3,
      4, 4, 4, 4, 4, 4, 4,
      5, 5, 5, 5, 5, 5,
      6, 6, 6, 6, 6,
      7, 7, 7, 7,
      8, 8, 8,
      9, 9,
      10
    ];
    this.x = WIDTH-70;
    this.y = 240;
    this.x_velocity = 0;
    this.x_velocity_constant = 3;
    this.y_velocity = 0;
    this.y_velocity_constant = 4;
    this.y_jump_accumlation = 0;
    this.on_cloud = false;
    this.current_cloud = null;
    this.width = 69;
    this.height = 66;

    this.collision_box_x = this.x + 15;
    this.collision_box_y = (this.y + this.height) - 11;
    this.collision_box_width = this.width/1.35;
    this.collision_box_height = 11;

    this.ram_state = {
      x_moving_left: false,
      x_moving_right: false,
      y_moving: false,
    };
    var self = this;
    this.controller_map = {
      left_up: {
        execute: function(controller) {
          self.ram_state.x_moving_left = false;
          if(!self.ram_state.x_moving_right) {
            self.x_velocity = 0;
            if(!self.ram_state.y_moving)
              self.sprite_cursor = 0;
            self.sprite_offset = 0;
            self.sprite_end = self.sprites.length/2;
          }
        }
      },
      left_down: {
        execute: function(controller) {
          self.ram_state.x_moving_left = true;
          self.x_velocity = -self.x_velocity_constant;
          self.sprite_offset = 0;
          self.sprite_end = self.sprites.length/2;
          self.delay_counter = self.delay_max-1;
          self.collision_box_x = self.x + 15;
          if(!self.ram_state.y_moving)
            self.sprite_cursor = 0;
          else
            self.sprite_cursor = self.sprite_end-1;
        }
      },
      right_up: {
        execute: function(controller) {
          self.ram_state.x_moving_right = false;
          if(!self.ram_state.x_moving_left) {
            self.x_velocity = 0;
            if(!self.ram_state.y_moving)
              self.sprite_cursor = 6;
            self.sprite_offset = 6;
            self.sprite_end = self.sprites.length;
          }
        }
      },
      right_down: {
        execute: function(controller) {
          self.ram_state.x_moving_right = true;
          self.x_velocity = self.x_velocity_constant;
          self.sprite_offset = 6;
          self.sprite_end = self.sprites.length;
          self.delay_counter = self.delay_max-1;
          self.collision_box_x = self.x+2;
          if(!self.ram_state.y_moving)
            self.sprite_cursor = 6;
          else
            self.sprite_cursor = self.sprite_end-1;
        }
      },
      up_up: {
        execute: function(controller) {
          if(self.y_fall_cursor < (self.y_fall_vector.length/2)-1) {
            if(!self.on_cloud) {
              self.y_fall_cursor = self.y_fall_vector.length/2;
            } else {
              self.y_fall_cursor = 0;
            }
          }
        }
      },
      up_down: {
        execute: function(controller) {
          self.ram_state.y_moving = true;
          self.current_cloud = null;
          self.on_cloud = false;
          self.sprite_cursor = self.sprite_end-1;
        }
      },
      pause: {
        execute: function(controller) {
          controller.target = menu;
          menu.renderable_menu = menu.pause_menu();
          menu.on = true;
          game_pause = true;
        }
      },
    };
  }

  move() {
    if(this.ram_state.y_moving) {
      if(this.y_fall_cursor < this.y_fall_vector.length) {
        this.y_velocity = this.y_fall_vector[this.y_fall_cursor];
        this.y_fall_cursor = this.y_fall_cursor+1;
      }
    }
    if(this.on_cloud) {
      if(!this.collision(this.current_cloud)) {
        this.current_cloud = null;
        this.ram_state.y_moving = true;
        this.on_cloud = false;
        this.sprite_cursor = this.sprite_end-1;
        this.y_fall_cursor = this.y_fall_vector.length/2;
      }
    }
    this.x = this.x + this.x_velocity;
    this.y = this.y + this.y_velocity;
    this.collision_box_x = this.collision_box_x + this.x_velocity;
    this.collision_box_y = this.collision_box_y + this.y_velocity;

    if(this.y > HEIGHT + 200) {
      this.x_velocity = 0;
      this.ram_state.x_moving_left = false;
      this.ram_state.x_moving_right = false;
      this.ram_state.y_moving = false;
      menu.on = true;
      game_over = true;
      menu.renderable_menu = menu.game_over_menu();
      controller.set_target(menu);
    }

  }

  render() {
    image(this.sprites[this.sprite_cursor], this.x, this.y);
    if((this.ram_state.x_moving_left || this.ram_state.x_moving_right)) {
      if(this.delay_counter === 7) {
        if(!this.ram_state.y_moving ) {
          this.sprite_cursor = this.sprite_offset + ((this.sprite_cursor + 1) % (this.sprite_end - this.sprite_offset));
        }
      }
      this.delay_counter = (this.delay_counter + 1) % this.delay_max;
    }
  }

  collision(obj) {
    var valid = false;
    if(this.collision_box_x < (obj.collision_box_x + obj.collision_box_width) &&
      (this.collision_box_x + this.collision_box_width) > obj.collision_box_x &&
      this.collision_box_y < (obj.collision_box_y + obj.collision_box_height) &&
      (this.collision_box_y + this.collision_box_height) > obj.collision_box_y) {
        valid = true;
    }
    return valid;
  }
}

var font = null;
var bg = null;
var fg = null;
var menu = null;
var ram = null;
var controller = null;
var renderables = [];

function setup() {
  frameRate(50);
  createCanvas(WIDTH, HEIGHT);
  bg = new Background();
  ram = new Ram();
  menu = new Menu();
  fg = new Foreground(ram);

  controller = new Controller();
  controller.set_target(menu);
  renderables.push(bg);
  renderables.push(menu);
  renderables.push(fg);
  bg.render();
}

function preload() {
  font = loadFont('font/PressStart2P-Regular.ttf');
}

function draw() {
  renderables.forEach(function(e, index, array) {
    e.render();
  });
}

function keyPressed() {
  controller.key_down(keyCode);
}

function keyReleased() {
  controller.key_up(keyCode);
}
